import React from "react";
import MyNumber from "./MyNumber/MyNumber";
import './App.css';

class App extends React.Component {
    state = {
        numbers: [
            {number: 1},
            {number: 2},
            {number: 3},
            {number: 4},
            {number: 5},
        ]
    };

    generateNumber = () => {
        return  Math.floor( Math.random() * (37 - 5) + 5);
    }

    createNumbers = () => {
        let arr = [];
        for (let i = 0; i < 5; i++) {

            if (i > 0) {
                while (true) {
                    const genNumber = this.generateNumber();
                    if(arr.includes(genNumber)) {
                        console.log('duplicate was found !')
                        continue;
                    } else {
                        arr[i] = genNumber;
                        break;
                    }
                }
            } else {
                arr[i] = this.generateNumber();
            }
        }

        return arr;
    }

    updateNumbers = () => {
        const sortedNumbers = this.createNumbers().sort(function(a, b){return a-b});
        const numbers = this.state.numbers.map((number, i) => {
            return {
                number: sortedNumbers[i]
            };
        });
        this.setState({numbers});
    };

    render() {
        return (
            <div className="App">
                <button onClick={this.updateNumbers} className="btn">
                    New numbers
                </button>
                <div className="numbers">
                    <MyNumber number={this.state.numbers[0].number}/>
                    <MyNumber number={this.state.numbers[1].number}/>
                    <MyNumber number={this.state.numbers[2].number}/>
                    <MyNumber number={this.state.numbers[3].number}/>
                    <MyNumber number={this.state.numbers[4].number}/>
                </div>
            </div>
        );
    }
}

export default App;
