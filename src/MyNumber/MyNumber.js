import React from 'react';

const MyNumber = props => {
    return (
        <div className="number">
            <i>{props.number}</i>
        </div>
    )
}

export default MyNumber;